<?php
/*
Template Name: video
*/


require_once($_SERVER['DOCUMENT_ROOT'] . "/wp-load.php");
getHeader();
getNavigation();
?>



<header>

</header>
<div id="site" class="container">
    <div class="intro">
        <h1>Persönliche Videoproduktion</h1>
        <?php getSiteContent("/videos/"); ?>
    </div>

    <div class="video-container">
        <h1>Esports News</h1>
        <div class="esport-youmax" ></div>
    </div>
    <div class="video-container">
        <h1>Private Videos</h1>
        <div class="jack-youmax" id="ymax"></div>
    </div>
    <div id="Videos mit Jack"></div>
</div>

<?php getFooter(); ?>

<script>

    jQuery(document).ready(function(){
        jQuery(".esport-youmax").youmaxPro({
            tabs:[
                {
                    type:"youtube-playlist-videos",
                    link:"https://www.youtube.com/playlist?list=PLasEfw3q-8dSn7IXrr8vI2Tw_4YUfhp4j",
                    name:"Esport News",
                },
            ],
            defaultTab:"Esport News",
            channelLinkForHeader:"https://www.youtube.com/channel/UCvG7RlkJ4HXiM83XREYXRBQ",
            apiKey:"AIzaSyAXKvGqWiGSFp3abqiU8kmAJi0sREcZXyU",
            youTubeClientId:"782255279334-jof4i49kaei3g2i3obodbjalcat0d7gd.apps.googleusercontent.com",
            vimeoAccessToken:"c289d754a132ca07051aaf931ef0de33",
            maxResults:"6",
            videoDisplayMode:"popup",
            playlistNavigation:true,
            youmaxDisplayMode:"grid",
            gridThumbnailType:"neat",
            autoPlay:true,
            dateFormat:"relative",
            hideHeader:false,
            hideTabs:false,
            hideSearch:false,
            hideSorting:false,
            hideViewSwitcher:false,
            hideLoadingMechanism:false,
            hideCtaButton:true,
            youmaxBackgroundColor:"#EEEEEE",
            itemBackgroundColor:"#fbfbfb",
            headerBackgroundColor:"#EEEEEE",
            headerTextColor:"white",
            titleColor:"#383838",
            descriptionColor:"828282",
            viewsColor:"#6f6f6f",
            tabsColor:"black",
            controlsTextColor:"black",
            baseFontSize:"16px",
            titleFontFamily:"Roboto Condensed",
            titleFontSize:"0.9",
            titleFontWeight:"normal",
            generalFontFamily:"sans-serif",
            descriptionFontSize:"0.84",
            viewsDateFontSize:"0.75",
            showHoverAnimation:false,
            showFixedPlayIcon:false,
            iconShape:"circle",
            loadingMechanism:"load-more",
            loadMoreText:"<i class=\"fa fa-plus\"></i>  Show me more videos..",
            previousButtonText:"<i class=\"fa fa-angle-left\"></i>  Previous",
            nextButtonText:"Next  <i class=\"fa fa-angle-right\"></i>",
            displayFirstVideoOnLoad:true,
            ctaLink:"https://gopro.com/",
            minimumViewsPerDayForTrendingVideos:"5",
            responsiveBreakpoints:[600,900,2000,2500],
        });
    });
    jQuery(document).ready(function(){
        jQuery(".jack-youmax").youmaxPro({
            tabs:[
                {
                    type:"youtube-channel-uploads",
                    link:"https://www.youtube.com/channel/UCKGU9rzLEOBqluQST9_Qnlg",
                    name:"Private Videos",
                },
            ],
            defaultTab:"Private Videos",
            channelLinkForHeader:"https://www.youtube.com/channel/UCKGU9rzLEOBqluQST9_Qnlg",
            apiKey:"AIzaSyAXKvGqWiGSFp3abqiU8kmAJi0sREcZXyU",
            youTubeClientId:"782255279334-jof4i49kaei3g2i3obodbjalcat0d7gd.apps.googleusercontent.com",
            vimeoAccessToken:"c289d754a132ca07051aaf931ef0de33",
            maxResults:"6",
            videoDisplayMode:"popup",
            playlistNavigation:true,
            youmaxDisplayMode:"grid",
            gridThumbnailType:"neat",
            autoPlay:true,
            dateFormat:"relative",
            hideHeader:false,
            hideTabs:false,
            hideSearch:false,
            hideSorting:false,
            hideViewSwitcher:false,
            hideLoadingMechanism:false,
            hideCtaButton:true,
            youmaxBackgroundColor:"#EEEEEE",
            itemBackgroundColor:"#fbfbfb",
            headerBackgroundColor:"#EEEEEE",
            headerTextColor:"white",
            titleColor:"#383838",
            descriptionColor:"828282",
            viewsColor:"#6f6f6f",
            tabsColor:"black",
            controlsTextColor:"black",
            baseFontSize:"16px",
            titleFontFamily:"Roboto Condensed",
            titleFontSize:"0.9",
            titleFontWeight:"normal",
            generalFontFamily:"sans-serif",
            descriptionFontSize:"0.84",
            viewsDateFontSize:"0.75",
            showHoverAnimation:false,
            showFixedPlayIcon:false,
            iconShape:"circle",
            loadingMechanism:"load-more",
            loadMoreText:"<i class=\"fa fa-plus\"></i>  Show me more videos..",
            previousButtonText:"<i class=\"fa fa-angle-left\"></i>  Previous",
            nextButtonText:"Next  <i class=\"fa fa-angle-right\"></i>",
            displayFirstVideoOnLoad:true,
            ctaLink:"https://gopro.com/",
            minimumViewsPerDayForTrendingVideos:"5",
            responsiveBreakpoints:[600,900,2000,2500],
        });
    });
</script>