<?php

/*
  Template Name: Games
 */


getHeader();
getNavigation();
?>

<div id="site" class="container">
    <div class="intro">
        <h1>Games von mir</h1>
        <?php getSiteContent("/games/"); ?>
    </div>
</div>



<?php getFooter(); ?>
