<?php

/*
  Template Name: Impressum
 */


getHeader();
getNavigation();
?>

<div id="site" class="container">
    <div class="intro">
        <h1>Impressum</h1>
        <?php getSiteContent("/impressum/"); ?>
    </div>
</div>



<?php getFooter(); ?>
