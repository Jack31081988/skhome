<?php
$isHome = isHome();

?>

<?php if($isHome) :?>
    <div class="navigation" id="home-nav">
        <?= wp_nav_menu(array('menu' => 'HomeNav')); ?>
    </div>

<?php else : ?>
    <div class="navigation" id="default-nav">
        <div class="container">
            <?= wp_nav_menu(array('menu' => 'Nav')); ?>
        </div>
    </div>

<?php endif; ?>
