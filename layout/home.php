<?php
/*
  Template Name: Home
 */
require_once($_SERVER['DOCUMENT_ROOT'] . "/wp-load.php");

function getLatestNewsPost()
{
    $args = array(
        'numberposts' => '1',
        'category_name' => 'News'
    );

    $posts = wp_get_recent_posts($args);
    $post = $posts[0];

    echo '<div><h1>Neugikeiten: </h1>' . date_i18n('F j, Y', strtotime($post['post_date'])) . '</div>';
    echo '<h3>' . $post['post_title'] . '</h3>';
    echo '<p>'.$post['post_content'].'</p>';
}

function getFotoTeaser()
{
    $media_query = new WP_Query(
        array(
            'post_type' => 'attachment',
            'post_status' => 'inherit',
            'posts_per_page' => 3,
        )
    );
    $list = array();
    foreach ($media_query->posts as $post) {
        $list[] = wp_get_attachment_url($post->ID);
    }

//create clickable pictures
    echo '<h1>Neusten Fotos</h1><div>';
    foreach ($list as $img) {
        echo '<div><img src="' . $img . '" class="img-responsive" ></div>';
    }
    echo '</div>';
}

getHeader();
?>

<header id="masthead" class="site-header" role="banner">

    <div class="custom-header" style="margin-bottom: 0px;">
        <div id="landing-page-background">
            <div class="blackshit"></div>
            <img class="landing-page-image" src="<?= get_template_directory_uri() ?>/public/img/IMG_5530.JPG"
                 alt="skHome"/>

            <div class="site-branding">
                <div class="site-branding-text" id="home-branding-text">
                    <a href="<?= $home_url ?>" rel="home">
                        <img src="<?= get_template_directory_uri() ?>/public/img/profilePic.jpg">
                    </a>
                    <div>
                        <h1 class="site-title">Stephan Knödler</h1>
                        <p class="site-description">Softwareentwickler und Hobby-Esport Blogger</p>
                    </div>

                </div><!-- .site-branding-text -->
                <?php getNavigation(); ?>

            </div><!-- .site-branding -->
            <a href="#site" id="downButton" class="menu-scroll-down "> <i class="fa fa-arrow-down"></i></a>
        </div>


    </div><!-- .custom-header -->


</header>
<div id="home">

    <div class="container">
        <!-- News -->
        <!-- Twitter/Facebook/Video Feed-->
        <div id="social" class="social-container">

            <div class="news-home">
                <?php getLatestNewsPost() ?>
            </div>

            <a href="/blog/"><i class="fa fa-arrow-right"></i>Zum Blog</a>
        </div>

        <!-- CV Teaser -->
        <div id="cv">
            <div class="cv-teaser-container">
                <h1>Wer ist Jack</h1>
                <div class="content">
                    <div>
                        <h2>Zitat: "Weil ich es Kann"</h2>
                        <p>Ausgebildet an der Technischen Hochschule Ingolstadt, Feuertaufe auf dem Schlachtfeld der Automobilindustrie</p>
                        <p>>Oracle, Websphere, Spring, JavaFX, WPF< egal ob mit einem Framework aus dem letzten Jahrtausend oder schlimmer, Eigenentwicklungen: "Been there, done that."</p>
                    </div>
                    <div class="coreskills">
                        <img src="<?= get_template_directory_uri() ?>/public/icon/arch.png">
                    </div>
                </div>
                <div class="content">
                    <div>
                        <h2>Problemlösung, by Design!</h2>
                        <p>Der wichtigste Skill eines Informatiker ist die Fähigkeit Probleme zu lösen. Design beschreibt nicht nur die oberflächliche Gestaltung! Design steht für den Plan einer Konstruktion </p>
                        <p>Je Besser der Plan, desto besser die resultierende Konstruktion. Doch für die Erstellung eines solchen Plan ist nicht immer einfach und Probleme müssen gelöst werden.</p>
                    </div>
                    <div class="coreskills">
                        <img src="<?= get_template_directory_uri() ?>/public/icon/hobbies.png">
                    </div>
                </div>
                <div class="content">
                    <div>
                        <h2>Der Ausgleich</h2>
                        <p>Jeder Mensch braucht einen Ausgleich. Mein Großvater hat mir einmal einen Rat gegeben, den ich nicht vergessen werde.</p>
                        <p> Ein Hobby sollte etwas sein, mit dem man sich Kreativ verausgaben kann</p>
                        <p>Kreativ sein, heißt nicht, dass man jetzt den Pinsel in die Hand nehmen muss. Kreativ sein heißt für mich, Gedanken und Träume in die Reale Welt zu bringen</p>
                    </div>
                    <div class="coreskills">
                        <img src="<?= get_template_directory_uri() ?>/public/icon/gamepad.png">
                    </div>
                </div>

            </div>
            <a href="/about/"><i class="fa fa-arrow-right"></i>Zum Lebenslauf</a>
        </div>
        <div id="foto" >
            <div class="foto-teaser">
                <?php getFotoTeaser() ?>
            </div>
            <a href="/fotos/"><i class="fa fa-arrow-right"></i>Zu den Fotos</a>
        </div>
        <!-- video -->
        <div id="video" >
            <div class="video-teaser">
                <h1>Esports News</h1>
                <div class="home-youmax" id="ymax"></div>
            </div>
            <a href="/videos/"><i class="fa fa-arrow-right"></i>zu den Videos</a>

        </div>
        <!-- Fotos Slideshow -->

    </div>

    <?php getFooter(); ?>
</div>


<script>

    jQuery(document).ready(function () {
        jQuery(".home-youmax").youmaxPro({
            tabs: [
                {
                    type: "youtube-playlist-videos",
                    link: "https://www.youtube.com/playlist?list=PLasEfw3q-8dSn7IXrr8vI2Tw_4YUfhp4j",
                    name: "Uploads",
                }
            ],
            defaultTab: "Uploads",
            apiKey: "AIzaSyBz6KUs6-Z4g41dDFWwhA_OrDICjLx-Sfs",
            youTubeClientId: "237485577723-lndqepqthdb3lh4gec2skvpfaii9sgh0.apps.googleusercontent.com",
            vimeoAccessToken: "c289d754a132ca07051aaf931ef0de33",
            maxResults: "3",
            videoDisplayMode: "popup",
            youmaxDisplayMode: "grid",
            gridThumbnailType: "neat",
            autoPlay: true,
            dateFormat: "relative",
            hideHeader: true,
            hideTabs: true,
            hideSearch: true,
            hideSorting: true,
            hideViewSwitcher: true,
            hideLoadingMechanism: true,
            hideCtaButton: true,
            youmaxBackgroundColor: "#ECEFF1",
            itemBackgroundColor: "#fbfbfb",
            headerColor: "rgb(252, 76, 74)",
            titleColor: "#383838",
            descriptionColor: "828282",
            viewsColor: "#6f6f6f",
            controlsTextColor: "black",
            baseFontSize: "16px",
            titleFontFamily: "Roboto Condensed",
            titleFontSize: "0.9",
            titleFontWeight: "normal",
            generalFontFamily: "sans-serif",
            descriptionFontSize: "0.84",
            viewsDateFontSize: "0.75",
            displayFirstVideoOnLoad: true,
            loadingMechanism: "load-more",
            loadMoreText: "<i class=\"fa fa-plus\"></i>  Show me more videos..",
            previousButtonText: "Previous",
            nextButtonText: "Next",
            ctaText: "",
            ctaLink: "",
            minimumViewsPerDayForTrendingVideos: "5",
            responsiveBreakpoints: [600, 900, 2000, 2500],
        });


        divAnimation('#social');
        divAnimation('#cv');
        divAnimation('#video', true);
        divAnimation('#foto');

    });
</script>
