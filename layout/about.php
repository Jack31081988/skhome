<?php

/*
  Template Name: About
 */


getHeader();
getNavigation();
?>

<div id="site" class="container">
    <div class="intro">
        <h1>Über mich</h1>
        <?php getSiteContent("/about/"); ?>
    </div>
</div>



<?php getFooter(); ?>
