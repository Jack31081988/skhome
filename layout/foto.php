<?php
/*
Template Name: Fotos
*/
function getFotos()
{
    $media_query = new WP_Query(
        array(
            'post_type' => 'attachment',
            'post_status' => 'inherit',
            'posts_per_page' => 3,
        )
    );
    $list = array();
    foreach ($media_query->posts as $post) {
        $list[] = wp_get_attachment_url($post->ID);
    }

//create clickable pictures
    foreach ($list as $img) {
        echo '<div><img src="' . $img . '" class="img-responsive" ></div>';
    }
    echo '</div>';
}

require_once($_SERVER['DOCUMENT_ROOT'] . "/wp-load.php");
getHeader();
getNavigation();

?>
<script type="text/javascript">
    var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
    photoAjaxCall();
    bindScroll();

    function bindScroll(){
        $(window).scroll(function (){
            if($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
                photoAjaxCall();
                $(window).unbind('scroll');
                setTimeout(bindScroll, 500);
            }
        });
    }

</script>



<div id="site" class="container">
    <div class="intro">
        <h1>Ich als Hobbyfotograf</h1>
        <?php getSiteContent("/fotos/"); ?>
    </div>
    <div class="foto-container" id="foto-gallery">

    </div>
    <button id="loadImages">Weiter bilder Laden</button>

</div>
<?php getFooter(); ?>
