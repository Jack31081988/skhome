
<?php
/*
Template Name: Blog
*/
/**
 * Created by PhpStorm.
 * User: Jack
 * Date: 28-Dec-16
 * Time: 15:23
 */

getHeader();
getNavigation();

?>
<script type="text/javascript">
    var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
    blogAjaxCall();
    bindScroll();

    function bindScroll(){
        $(window).scroll(function (){
            if($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
                blogAjaxCall();
                $(window).unbind('scroll');
                setTimeout(bindScroll, 500);
            }
        });
    }

</script>

<div id="site" class="container">

    <div id="blog-entries">

    </div>
</div>


<?php
getFooter();
?>
