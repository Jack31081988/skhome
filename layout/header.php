<?php

global $siteInit;

if($siteInit == false){
    initSite();
}
?>
<!DOCTYPE HTML>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="<?= get_stylesheet_uri() ?>" />
    <script src="<?= get_template_directory_uri() ?>/js/jquery.min.js"></script>
    <script src="<?= get_template_directory_uri() ?>/js/scripts.min.js"></script>
</head>
<body>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-89577930-1', 'auto');
    ga('send', 'pageview');

</script>
