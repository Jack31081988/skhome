module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        less: {
            options: {
                compile: true,
                compress: true
            },
            style: {
                src: ['less/style.less'],
                dest: 'css/style.css'
            }
        },

        concat: {
            default: {
                src: [
                    'css/theme-header.css',
                    'css/youmax-pro.min.css',
                    'css/style.css',

                ],
                dest: 'style.css'
            },
            scripts: {
                src: [
                    'js/basic.js',
                    'js/youmax-pro.min.js'
                ],
                dest: 'js/scripts.js'
            }

        },

        uglify: {
            scripts: {
                src: ['<%= concat.scripts.dest %>'],
                dest: 'js/scripts.min.js'
            }
        },

        watch: {
            scripts: {
                files: '<%= concat.scripts.src %>',
                tasks: ['dist-js']
            },
            less: {
                files: ['less/*.less', 'less/bootstrap/*.less', 'less/awesomefont/*.less'],
                tasks: ['dist-css', 'dist']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');

    grunt.registerTask('dist-js', ['concat', 'uglify']);
    grunt.registerTask('dist-css', ['less']);
    grunt.registerTask('dist', ['dist-js', 'dist-css']);
    grunt.registerTask('default', ['dist']);
};