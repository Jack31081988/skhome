/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
// A $( document ).ready() block.
function getDocHeight() {
    var D = document;
    return Math.max(
        D.body.scrollHeight, D.documentElement.scrollHeight,
        D.body.offsetHeight, D.documentElement.offsetHeight,
        D.body.clientHeight, D.documentElement.clientHeight
    );
}


$(document).ready(function () {
    //hello



    $("#home-branding-text").animate({opacity: '1', marginBottom: '60px'}, 'slow');
    $("#landing-page-background .site-branding .navigation .menu-homenav-container ul>li>a").animate({opacity: '1', top: '0'}, 'slow');


    $('#loadImages').on('click',photoAjaxCall );

});

function skillAnimation(elementID, parcentage) {
    $(window).scroll(function () {
        var bottom_of_object = $(elementID).offset().top;
        var bottom_of_window = $(window).scrollTop() + ($(window).height());
        console.log("appear " + bottom_of_window + " >? " + bottom_of_object);
        /* If the object is completely visible in the window, fade it it */
        if (bottom_of_window > bottom_of_object) {
            console.log("appear" + elementID);
            $(elementID).animate({width: parcentage}, 5000);
            $(elementID).unbind('scroll');

        }
    });

}

function divAnimation(elementID, unbind){


    $(window).scroll(function () {
        var bottom_of_object = $(elementID).offset().top;
        var bottom_of_window = $(window).scrollTop() + ($(window).height());

        /* If the object is completely visible in the window, fade it it */
        if (bottom_of_window > bottom_of_object) {
            console.log("appear" + $(this));
            $(elementID).animate({'opacity': '1'}, (2000));

            if(unbind){
                $(window).unbind('scroll');
            }

        }
    });

}


var photocounter = 0;
function photoAjaxCall() {
    $.ajax({
        url: ajaxurl,
        type: "POST",
        cache: false,
        data: 'counter=' + photocounter + '&action=getPhotos', //action defines which function to use in add_action

        success: function (data) {
            photocounter += 9;
            flag = true;
            data = $.parseJSON(data);
            $.each(data, function (i, imgDiv) {
                $('#foto-gallery').append(imgDiv);
            });
            $('#foto-gallery').find('.main-photo').animate({'opacity': '1'}, 2000);
            bindPhotoCallback();

        },
        error: function (data) {
            flag = true;
            no_data = false;

            alert("nix gut:" + data.responseText);
        }
    });
}


function bindPhotoCallback() {
    $(".photo-small").each(function () {
        if(!$(this).hasClass("bound")){
            $(this).addClass("bound");
            $(this).on('click', function (e) {
                e.preventDefault();
                $(this).find(".modal").addClass("show");

                console.log("appear");
                $(this).find(".modal").animate({'opacity': '1'}, 600);
                $(this).find("img.modal-content").attr('src', $(this).find("img.main-photo").attr('src'));
                $(this).unbind('click');
                $(this).removeClass("bound");
                setTimeout(bindPhotoCloseCallback, 600, $(this));
            })
        }
    })
}

function bindPhotoCloseCallback(parent){
    parent.find(".modal").on('click', function (e) {
        e.preventDefault();

        $(this).unbind('click');
        parent.find(".modal").animate({'opacity': '0'}, 600);
        console.log("start removing");

        setTimeout(removeShow, 600, $(this));


    });
}

function removeShow(parent){
    console.log("remove");
    parent.removeClass("show");
    setTimeout(bindPhotoCallback, 210);
}


var blogcounter = 0;
function blogAjaxCall() {
    $.ajax({
        url: ajaxurl,
        type: "POST",
        cache: false,
        data: 'counter=' + blogcounter + '&action=getBlog', //action defines which function to use in add_action

        success: function (data) {
            blogcounter += 1;
            flag = true;
            $('#blog-entries').append(data);
            $('#blog-entries').find('.blog-entry').animate({'margin-top': '40px'}, 1000);

        },
        error: function (data) {
            flag = true;
            no_data = false;

            alert("nix gut:" + data.responseText);
        }
    });
}