<?php
global $wp;
global $current_url;
global $home_url;
global $siteInit;


function initSite()
{
    global $wp;
    global $current_url;
    global $home_url;
    global $siteInit;

    $siteInit = true;

    $current_url = home_url(add_query_arg(array(), $wp->request));
    $home_url = get_home_url();
}

function isHome()
{
    global $current_url;
    global $home_url;
    if ($current_url === $home_url) {
        return true;
    } else {
        if ($current_url == "$home_url/home") {
            return true;
        } else {
            return false;
        }
    }
}

function getHeader()
{

    require_once(get_template_directory() . "/layout/header.php");
}

function getNavigation()
{
    require_once(get_template_directory() . "/layout/navigation.php");
}

function getFooter()
{
    require_once(get_template_directory() . "/layout/footer.php");
}

function getSiteContent($path)
{
    $page = get_page_by_path($path);
    $content = apply_filters('the_content', $page->post_content);
    echo $content;

}

function getNextPhotoUrls()
{
    $val0 = $_POST['counter'];

    $media_query = new WP_Query(
        array
        (
            'post_type' => 'attachment',
            'post_mime_type' => 'image',
            'post_status' => 'inherit',
            'posts_per_page' => -1,
        )
    );
    $list = array();
    foreach ($media_query->posts as $post) {
        $list[] = wp_get_attachment_url($post->ID);
    }


    $counter = 0;
    $returnList = array();
    foreach ($list as $img) {
        if($counter < $val0){
            $counter++;
            continue;
        }else{
            $counter++;
        }
        $returnList[] = '<div class="photo-small">
                            <img class="main-photo" src="' . $img . '" >
                            <div class="modal">
                                <img class="modal-content">
                            </div>
                        </div>';

        if($counter == $val0 + 9){
            break;
        }

    }

    echo json_encode($returnList);
    wp_die();
}

add_action('wp_ajax_getPhotos', 'getNextPhotoUrls');
add_action('wp_ajax_nopriv_getPhotos', 'getNextPhotoUrls');

function getNextBlogEntry(){
    $val0 = $_POST['counter'];

    $args = array
        (
            'category' => '3',
            'numberposts' => -1,
        );

    $post_list = get_posts($args);
    $counter = 0;
    foreach ($post_list as $post) {

        if($counter < $val0){
            $counter++;
            continue;
        }else{
            $counter++;
        }
        echo '<div class="blog-entry">
                    <div class="blog_entry_header">
                        <h2>'.$post->post_title.'</h2>
                        <div class="blog_entry_info">';

        foreach ($post->post_category as $category){
            if($category == 3){
                continue;
            }
            echo '<div>'.get_the_category_by_ID($category).'</div>';
        }
        echo '<div>' . date_i18n('F j, Y', strtotime($post->post_date)) . '</div>
                        </div>
                    </div>
                    <p>'.$post->post_content.'</p>
                </div>
                ';
        if($counter == $val0 + 1){
            break;
        }

    }

    wp_die();
}

add_action('wp_ajax_getBlog', 'getNextBlogEntry');
add_action('wp_ajax_nopriv_getBlog', 'getNextBlogEntry');